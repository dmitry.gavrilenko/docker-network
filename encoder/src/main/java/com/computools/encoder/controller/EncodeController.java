package com.computools.encoder.controller;

import com.computools.encoder.dto.Message;
import com.computools.encoder.service.EncodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EncodeController {

    @Autowired
    private EncodeService encodeService;

    @PostMapping("/encode")
    public Message encode(@RequestBody Message message) {
        return new Message(encodeService.encode(message));
    }

}
