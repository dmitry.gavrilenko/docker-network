package com.computools.encoder.service;

import com.computools.encoder.dto.Message;
import org.springframework.stereotype.Service;

import java.util.Base64;

@Service
public class Base64EncodeService implements EncodeService {

    @Override
    public String encode(Message message) {
        return new String(Base64.getEncoder().encode(message.message.getBytes()));
    }

}
