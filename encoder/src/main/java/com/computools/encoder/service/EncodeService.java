package com.computools.encoder.service;

import com.computools.encoder.dto.Message;

public interface EncodeService {

    String encode(Message message);

}
