package com.computools.sender.controller;

import com.computools.sender.dto.Message;
import com.computools.sender.service.PersistMessageService;
import com.computools.sender.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendController {

    @Autowired
    private SendService sendService;

    @Autowired
    private PersistMessageService persistMessageService;

    @PostMapping("/send")
    public Message send(@RequestBody Message message) {
        if (persistMessageService.save(message)) {}
        return sendService.send(message);
    }

}
