package com.computools.sender.dto;

public class Message {

    public String message;

    public Message(String message) {
        this.message = message;
    }

    public Message() {}
}
