package com.computools.sender.service;

import com.computools.sender.dto.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Base64SendService implements SendService {

    @Value("${url}")
    private String url;

    @Override
    public Message send(Message message) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Message> response = restTemplate.postForEntity(url, message, Message.class);
        return response.getBody();
    }

}
