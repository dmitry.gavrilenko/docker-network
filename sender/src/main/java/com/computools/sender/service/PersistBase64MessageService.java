package com.computools.sender.service;

import com.computools.sender.dto.Message;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class PersistBase64MessageService implements PersistMessageService {

    @Override
    public boolean save(Message message) {
        try(FileOutputStream fio = new FileOutputStream("/var/docker/encode.txt")) {
            fio.write(message.message.getBytes());
        }  catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
