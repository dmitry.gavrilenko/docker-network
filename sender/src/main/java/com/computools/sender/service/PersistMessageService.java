package com.computools.sender.service;

import com.computools.sender.dto.Message;

public interface PersistMessageService {

    boolean save(Message message);

}
