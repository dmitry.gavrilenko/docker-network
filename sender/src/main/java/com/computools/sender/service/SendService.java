package com.computools.sender.service;

import com.computools.sender.dto.Message;

public interface SendService {

    Message send(Message message);

}
